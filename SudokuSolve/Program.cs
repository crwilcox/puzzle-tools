﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Christopher Wilcox">
// All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace SudokuSolve
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This is the entry class for SudokuSolve
    /// </summary>
    public class Program
    {
        /// <summary>
        /// This is the main method for SudokuSolve
        /// </summary>
        /// <param name="args">The arguments provided from the command line</param>
        public static void Main(string[] args)
        {
            string path;
            if (args.Length == 0)
            {
                Console.WriteLine("Please Provide a path to a sudoku puzzle (ie. puzzle.txt)");
                path = Console.ReadLine();
            }
            else
            {
                path = args[0];
            }

            SudokuPuzzle sp = new SudokuPuzzle(path);
            Console.WriteLine("Provided Puzzle:");
            sp.PrintMatrix();

            sp.SolveMatrix();

            Console.WriteLine();
            Console.WriteLine("Solution:");
            sp.PrintMatrix();
        }
    }
}
