﻿// -----------------------------------------------------------------------
// <copyright file="SudokuPuzzle.cs" company="Christopher Wilcox">
// All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace SudokuSolve
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This is the work horse of sudoku solve.  this contains all methods
    /// that do real work such as solving and printing the puzzle.
    /// </summary>
    public class SudokuPuzzle
    {
        /// <summary>
        /// The path to the provided sudoku puzzle
        /// </summary>
        private string path;

        /// <summary>
        /// The matrix representing the sudoku puzzle
        /// </summary>
        private int[][] matrix;

        /// <summary>
        /// Initializes a new instance of the SudokuPuzzle class.  Given a path to a puzzle, 
        /// it attempts to load it into memory.
        /// </summary>
        /// <param name="path">path to a sudoku puzzle text file</param>
        public SudokuPuzzle(string path)
        {
            this.path = path;
            this.FillMatrix(path);
        }

        /// <summary>
        /// This method will print the matrix as it is known with 
        /// the formatting of a sudoku board
        /// </summary>
        public void PrintMatrix()
        {
            for (int i = 0; i < 9; i++)
            {
                if (i % 3 == 0)
                {
                    Console.WriteLine("+---+---+---+");
                }

                for (int j = 0; j < 9; j++)
                {
                    if (j % 3 == 0)
                    {
                        Console.Write("|");
                    }

                    int matVal = this.matrix[i][j];

                    if (matVal == 0)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write(this.matrix[i][j]);
                    }
                }

                Console.Write("|");
                Console.WriteLine();
            }

            Console.WriteLine("+---+---+---+");
        }

        /// <summary>
        /// This is the access point to solve the puzzle
        /// </summary>
        public void SolveMatrix()
        {
            int maxIterations = 82; // Shouldn't need more than 81, but hey
            int iterations = 0;

            // while all squares are not solved (any equal 0)
            while (!this.MatrixSolved() && iterations < maxIterations)
            {
                iterations++;

                // Check each square for possibilities.  Then add the list of
                // possible values found to a master list to be used later
                List<List<int>> possiblesFullMatrix = new List<List<int>>();
                for (int row = 0; row < 9; row++)
                {
                    for (int col = 0; col < 9; col++)
                    {
                        // check each square
                        List<int> ret = this.CheckSquare(row, col);
                        possiblesFullMatrix.Add(ret);
                    }
                }

                // now that we have lists of each squares possiblities, 
                // we need to see if any squares have unique solutions
                // which would allow us to solve the square.
                this.CheckForUniqueSolutions(possiblesFullMatrix);
            }
        }

        /// <summary>
        /// This is a helper method used by the constructor to help fill
        /// the matrix using the provided path
        /// </summary>
        /// <param name="path">path to a text file containing a sudoku puzzle</param>
        private void FillMatrix(string path)
        {
            this.matrix = new int[9][];
            for (int i = 0; i < 9; i++)
            {
                this.matrix[i] = new int[9];
            }

            FileStream sudokuFile = File.OpenRead(path);
            StreamReader sr = new StreamReader(sudokuFile);
            for (int i = 0; i < 9; i++)
            {
                string line = sr.ReadLine();
                for (int j = 0; j < 9; j++)
                {
                    char singleEntry = line.ToCharArray()[j];
                    if (singleEntry == '-')
                    {
                        singleEntry = '0';
                    }

                    this.matrix[i][j] = int.Parse(singleEntry.ToString());
                }
            }
        }

        /// <summary>
        /// This method is a helper of SolveMatrix
        /// It is used to build groups and then compare the possible
        /// solutions in each group.  If a unique solution can be found
        /// for any square, it will be used and modify the matrix
        /// </summary>
        /// <param name="possiblesFullMatrix">a list of lists of possible values of squares</param>
        private void CheckForUniqueSolutions(List<List<int>> possiblesFullMatrix)
        {
            // Now, we need to compare all possible squares in groups to see if we have a choice
            List<int>[] pfm = possiblesFullMatrix.ToArray();
            List<int>[] grp1, grp2, grp3, grp4, grp5, grp6, grp7, grp8, grp9;

            // call build groups to make our groups
            grp1 = this.BuildGroup(pfm, 0);
            grp2 = this.BuildGroup(pfm, 3);
            grp3 = this.BuildGroup(pfm, 6);
            grp4 = this.BuildGroup(pfm, 27);
            grp5 = this.BuildGroup(pfm, 30);
            grp6 = this.BuildGroup(pfm, 33);
            grp7 = this.BuildGroup(pfm, 54);
            grp8 = this.BuildGroup(pfm, 57);
            grp9 = this.BuildGroup(pfm, 60);

            // now that we have each group, we can analyze them.
            // take each group, and see if we can't find a unique value 
            // for a square
            this.FindUniqueSolutionsInGroup(grp1, 0);
            this.FindUniqueSolutionsInGroup(grp2, 3);
            this.FindUniqueSolutionsInGroup(grp3, 6);
            this.FindUniqueSolutionsInGroup(grp4, 27);
            this.FindUniqueSolutionsInGroup(grp5, 30);
            this.FindUniqueSolutionsInGroup(grp6, 33);
            this.FindUniqueSolutionsInGroup(grp7, 54);
            this.FindUniqueSolutionsInGroup(grp8, 57);
            this.FindUniqueSolutionsInGroup(grp9, 60);
        }

        /// <summary>
        /// This is used to find the unique solutions within a group
        /// </summary>
        /// <param name="grp">The Group we want to find a unique solution for</param>
        /// <param name="baseSquare">the base square of the group (ie 0, 3, 6, 27, etc)</param>
        private void FindUniqueSolutionsInGroup(List<int>[] grp, int baseSquare)
        {
            // we use the  helper Method to check against possibles of all other squares
            // now we need to find the unique squares.
            this.CheckSquareAgainstRestOfGroup(grp, 0, baseSquare);
            this.CheckSquareAgainstRestOfGroup(grp, 1, baseSquare + 1);
            this.CheckSquareAgainstRestOfGroup(grp, 2, baseSquare + 2);

            baseSquare += 9; // move down a row
            this.CheckSquareAgainstRestOfGroup(grp, 3, baseSquare);
            this.CheckSquareAgainstRestOfGroup(grp, 4, baseSquare + 1);
            this.CheckSquareAgainstRestOfGroup(grp, 5, baseSquare + 2);

            baseSquare += 9; // move down a row
            this.CheckSquareAgainstRestOfGroup(grp, 6, baseSquare);
            this.CheckSquareAgainstRestOfGroup(grp, 7, baseSquare + 1);
            this.CheckSquareAgainstRestOfGroup(grp, 8, baseSquare + 2); 
        }

        /// <summary>
        ///  we use this helper Method to check against possibles of all other squares
        /// </summary>
        /// <param name="grp">the current group being checked</param>
        /// <param name="sqrGroup">the square (0-8) in the group we are checking</param>
        /// <param name="sqrMatrix">the numerical square in the matrix we are checking</param>
        private void CheckSquareAgainstRestOfGroup(List<int>[] grp, int sqrGroup, int sqrMatrix)
        {
            /*
             * combine all other squares and check against my list, if nothing is found, or my list
             * is 1 items long, we can change that element.        
             */

            if (grp[sqrGroup].Count == 1)
            {
                // we can just change this in the matrix and return
                this.matrix[sqrMatrix / 9][sqrMatrix % 9] = grp[sqrGroup][0];
                return;
            }
            else
            {
                List<int> combinedPossiblesOfOtherSquares = new List<int>();

                // sum all squares lists in the group, except the one we are on
                for (int i = 0; i < 9; i++)
                {
                    if (i != sqrGroup)
                    {
                        foreach (int j in grp[i])
                        {
                            combinedPossiblesOfOtherSquares.Add(j);
                        }
                    }
                }

                /*
                 * then we compare the elements in our list.  if we find a unique one, it 
                 * must be the correct solution
                 */
                foreach (int possSolution in grp[sqrGroup])
                {
                    if (combinedPossiblesOfOtherSquares.Contains(possSolution))
                    {
                        // not unique.  we cannot add this with certainty
                    }
                    else
                    {
                        // is unique.  Lock this value in.
                        this.matrix[sqrMatrix / 9][sqrMatrix % 9] = possSolution;
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// this builds an array of the lists of a particular group
        /// </summary>
        /// <param name="pfm">the possibleMatrix that was found using checkSquare</param>
        /// <param name="baseSquare">the base square of the group </param>
        /// <returns>an array of lists representing the group</returns>
        private List<int>[] BuildGroup(List<int>[] pfm, int baseSquare)
        {
            List<int>[] retArr = new List<int>[9];

            retArr[0] = pfm[baseSquare];
            retArr[1] = pfm[baseSquare + 1];
            retArr[2] = pfm[baseSquare + 2];

            baseSquare = baseSquare + 9;
            retArr[3] = pfm[baseSquare];
            retArr[4] = pfm[baseSquare + 1];
            retArr[5] = pfm[baseSquare + 2];
            
            baseSquare = baseSquare + 9;
            retArr[6] = pfm[baseSquare];
            retArr[7] = pfm[baseSquare + 1];
            retArr[8] = pfm[baseSquare + 2];

            return retArr;
        }

        /// <summary>
        /// A boolean check if the matrix is solved
        /// </summary>
        /// <returns>True if the matrix is solved</returns>
        private bool MatrixSolved()
        {
            for (int row = 0; row < 9; row++)
            {
                for (int col = 0; col < 9; col++)
                {
                    if (this.matrix[row][col] == 0)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// This checks a single square for possible values
        /// </summary>
        /// <param name="row">row of the square (0-8)</param>
        /// <param name="col">col of the square (0-8)</param>
        /// <returns>a list of possible integer values for the square.</returns>
        private List<int> CheckSquare(int row, int col) 
        {
            int currentVal = this.matrix[row][col];
            List<int> possibles = new List<int>();

            // if square is non zero, just return its value
            if (this.matrix[row][col] != 0)
            {
                possibles.Add(this.matrix[row][col]);
                return possibles;
            }
            else
            {
                // else, we need to find the best choice
                // start thinking anything is a possibility
                for (int i = 1; i <= 9; i++)
                {
                    possibles.Add(i);
                }

                // check row across.  If we find a number, it is not a possibility
                for (int i = 0; i < 9; i++)
                {
                    // just try to remove every row value
                    possibles.Remove(this.matrix[row][i]);
                }

                // check column down.  If we find a number, it is not a possibility
                for (int i = 0; i < 9; i++)
                {
                    // just try to remove every column value
                    possibles.Remove(this.matrix[i][col]);
                }

                // check cluster it is in.  If we find a number, it is not a possibility.
                List<int> itemsInCluster = this.GetClusterItems(row, col);
                foreach (int i in itemsInCluster)
                {
                    possibles.Remove(i);
                }

                return possibles;
            }
        }

        /// <summary>
        /// This helper provides the known values for the cluster, or group
        /// the row and column of the item are used to find what group to get get
        /// </summary>
        /// <param name="row">the row of the item</param>
        /// <param name="col">the column of the item</param>
        /// <returns>a list of ints representing values in that cluster/group</returns>
        private List<int> GetClusterItems(int row, int col)
        {
            List<int> retList = new List<int>();
            int rowStart, rowEnd, colStart, colEnd;

            if (row < 3)
            {
                rowStart = 0;
                rowEnd = 3;
            }
            else if (row < 6)
            {
                rowStart = 3;
                rowEnd = 6;
            }
            else 
            {
                // if (row < 9)
                rowStart = 6;
                rowEnd = 9;
            }

            if (col < 3)
            {
                colStart = 0;
                colEnd = 3;
            }
            else if (col < 6)
            {
                colStart = 3;
                colEnd = 6;
            }
            else 
            {
                // if (col < 9)
                colStart = 6;
                colEnd = 9;
            }

            for (int i = rowStart; i < rowEnd; i++)
            {
                for (int j = colStart; j < colEnd; j++)
                {
                    retList.Add(this.matrix[i][j]);
                }
            }

                return retList;
        }
    }
}
