﻿namespace ConvertMorseCode
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    class Program
    {
        class Progress
        {
            public string Word;
            public string Morse;

            public Progress(string word, string morse)
            {
                this.Word = word;
                this.Morse = morse;
            }
        }
        #region morse lists
        private static string[,] morse1 = 
        {
                                { "E", "." },
                                { "T", "-" }
        };

        private static string[,] morse2 = 
        {
                                { "A", ".-" },
                                { "I", ".." },
                                { "M", "--" },
                                { "N", "-." }
        };

        private static string[,] morse3 = 
        {
                                { "D", "-.." },
                                { "G", "--." },
                                { "K", "-.-" },
                                { "O", "---" },
                                { "R", ".-." },
                                { "S", "..." },
                                { "U", "..-" },
                                { "W", ".--" }
        };

        private static string[,] morse4 = 
        {
                                { "B", "-..." },
                                { "C", "-.-." },
                                { "F", "..-." },
                                { "H", "...." },
                                { "J", ".---" },
                                { "L", ".-.." },
                                { "P", ".--." },
                                { "Q", "--.-" },
                                { "V", "...-" },
                                { "X", "-..-" },
                                { "Y", "-.--" },
                                { "Z", "--.." }
        };
        #endregion morse lists

        static void Main(string[] args)
        {
            // single letters
            TestMethod(".", "E");
            TestMethod("-", "T");
            TestMethod("...", "S");
            TestMethod("---", "O");
            TestMethod(".-", "A");
            TestMethod("-...", "B");
            TestMethod("-.-.", "C");
            TestMethod("-..", "D");

            // random words
            TestMethod(".-----.-.-..", "WORD");
            TestMethod("......-.-.-", "HEART");
            TestMethod(".....", "HE");
            TestMethod("...---...", "SOS");

            Console.ReadLine();
        }
        
        /// <summary>
        /// This is a test method to verify the functionality of findMorse
        /// </summary>
        /// <param name="input"></param>
        /// <param name="expectedOutput"></param>
        public static void TestMethod(string input, string expectedOutput)
        {
            List<string> received = FindMorse(input);
            bool pass = received.Contains(expectedOutput);

            Console.WriteLine("Input: " + input);
            Console.WriteLine("Expect: " + expectedOutput);

            if (pass)
            {
                Console.WriteLine("PASSED");
            }
            else
            {
                Console.WriteLine("FAILED");
                Console.WriteLine("Received: ");
                foreach (string item in received)
                {
                    Console.WriteLine(item);
                }
            }

            Console.WriteLine();
        }

        /// <summary>
        /// Given a morse string, this code produces all of the possibilities of it
        /// </summary>
        /// <param name="givenMorse"></param>
        /// <returns></returns>
        public static List<string> FindMorse(string givenMorse)
        {
            // a list to put the words that are finished and all replacement has been done
            List<string> completeWords = new List<string>();

            // a list where words that aren't finished will go
            List<Progress> incomplete = new List<Progress>();
            incomplete.Add(new Progress(string.Empty, givenMorse));

            // while there are incomplete words
            while (incomplete.Count != 0)
            {
                // remove a word from incomplete. 
                Progress inc = incomplete[0];
                incomplete.RemoveAt(0);

                // now try to process the different lengths, placing complete and incomplete items into corresponding lists
                if (inc.Morse.Length >= 4)
                {
                    Progress loc = new Progress(inc.Word, inc.Morse);
                    string f = Find4(loc.Morse);
                    loc.Morse = loc.Morse.Substring(4); // remove the just searched text
                    loc.Word += f; // add the new character

                    if (loc.Morse.Length == 0)
                    {
                        completeWords.Add(loc.Word); // done so add to complete list
                    }
                    else
                    {
                        incomplete.Add(loc); // not done, add our findings back to the list
                    }
                }

                if (inc.Morse.Length >= 3)
                {
                    Progress loc = new Progress(inc.Word, inc.Morse);
                    string f = Find3(loc.Morse);
                    loc.Morse = loc.Morse.Substring(3); // remove the just searched text
                    loc.Word += f; // add the new character

                    if (loc.Morse.Length == 0)
                    {
                        completeWords.Add(loc.Word); // done so add to complete list
                    }
                    else
                    {
                        incomplete.Add(loc); // not done, add our findings back to the list
                    }
                }

                if (inc.Morse.Length >= 2)
                {
                    Progress loc = new Progress(inc.Word, inc.Morse);
                    string f = Find2(loc.Morse);
                    loc.Morse = loc.Morse.Substring(2); // remove the just searched text
                    loc.Word += f; // add the new character

                    if (loc.Morse.Length == 0)
                    {
                        completeWords.Add(loc.Word); // done so add to complete list
                    }
                    else
                    {
                        incomplete.Add(loc); // not done, add our findings back to the list
                    }
                }

                if (inc.Morse.Length >= 1)
                {
                    Progress loc = new Progress(inc.Word, inc.Morse);
                    string f = Find1(loc.Morse);
                    loc.Morse = loc.Morse.Substring(1); // remove the just searched text
                    loc.Word += f; // add the new character

                    if (loc.Morse.Length == 0)
                    {
                        completeWords.Add(loc.Word); // done so add to complete list
                    }
                    else
                    {
                        incomplete.Add(loc); // not done, add our findings back to the list
                    }
                }
            }

            return completeWords;
        }
        
        #region Private Methods
        /// <summary>
        /// Given a string, this returns the character represented by the 
        /// first 4 characters of the morse string
        /// </summary>
        /// <param name="morse"></param>
        /// <returns></returns>
        private static string Find4(string morse)
        {
            if (morse.Length >= 4)
            {
                for (int i = 0; i < morse4.GetLength(0); i++)
                {
                    if (morse.Substring(0, 4) == morse4[i, 1])
                    {
                        return morse4[i, 0];
                    }
                }
            }

            return null;
        }
        
        /// <summary>
        /// Given a string, this returns the character represented by the 
        /// first 3 characters of the morse string
        /// </summary>
        /// <param name="morse"></param>
        /// <returns></returns>
        private static string Find3(string morse)
        {
            if (morse.Length >= 3)
            {
                for (int i = 0; i < morse3.GetLength(0); i++)
                {
                    if (morse.Substring(0, 3) == morse3[i, 1])
                    {
                        return morse3[i, 0];
                    }
                }
            }

            return null;
        }
        
        /// <summary>
        /// Given a string, this returns the character represented by the 
        /// first 2 characters of the morse string
        /// </summary>
        /// <param name="morse"></param>
        /// <returns></returns>
        private static string Find2(string morse)
        {
            if (morse.Length >= 2)
            {
                for (int i = 0; i < morse2.GetLength(0); i++)
                {
                    if (morse.Substring(0, 2) == morse2[i, 1])
                    {
                        return morse2[i, 0];
                    }
                }
            }

            return null;
        }
        
        /// <summary>
        /// Given a string, this returns the character represented by the 
        /// first character of the morse string
        /// </summary>
        /// <param name="morse"></param>
        /// <returns></returns>
        private static string Find1(string morse)
        {
            if (morse.Length >= 1)
            {
                for (int i = 0; i < morse1.GetLength(0); i++)
                {
                    if (morse.Substring(0, 1) == morse1[i, 1])
                    {
                        return morse1[i, 0];
                    }
                }
            }

            return null;
        }
        #endregion Private Methods
    }
}
