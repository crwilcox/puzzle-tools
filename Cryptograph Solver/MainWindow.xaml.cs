﻿namespace Cryptograph_Solver
{
    using System;
    using System.Windows;
    /*
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
     */

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void EnterText_Click(object sender, RoutedEventArgs e)
        {
            string output = string.Empty;

            // make array from the values in the boxes
            string[] arr = 
            {
                textBoxA.Text, textBoxB.Text, textBoxC.Text, textBoxD.Text, textBoxE.Text, 
                textBoxF.Text, textBoxG.Text, textBoxH.Text, textBoxI.Text, textBoxJ.Text, 
                textBoxK.Text, textBoxL.Text, textBoxM.Text, textBoxN.Text, textBoxO.Text,
                textBoxP.Text, textBoxQ.Text, textBoxR.Text, textBoxS.Text, textBoxT.Text,
                textBoxU.Text, textBoxV.Text, textBoxW.Text, textBoxX.Text, textBoxY.Text, textBoxZ.Text
            };

            // take all of the typed boxes.  if a character exists, replace it.  if not, put blanks where a character should be
            foreach (char c in InputBox.Text.ToUpper())
            {
                if (c.Equals(' ')) 
                { 
                    output += ' '; 
                }
                else
                {
                    // now we need to take the list of items and do replacements
                    output += arr[c - 'A'];
                }
            }

            OutputBox.Text = output;
        }

        private void FindCounts_Click(object sender, RoutedEventArgs e)
        {
            // Get the Counts
            int[] count = new int[26];
            foreach (char c in InputBox.Text.ToUpper())
            {
                if (!c.Equals(' '))
                {
                    count[c - 'A']++;
                }
            }

            // now that we have counts, we can enable/disable entry boxes as need be
            // disable all unnecessary buttons to make the use of this easier
            // a box is disabled if there were no characters of that type found
            textBoxA.IsEnabled = count[0] > 0;
            textBoxB.IsEnabled = count[1] > 0;
            textBoxC.IsEnabled = count[2] > 0;
            textBoxD.IsEnabled = count[3] > 0;
            textBoxE.IsEnabled = count[4] > 0;
            textBoxF.IsEnabled = count[5] > 0;
            textBoxG.IsEnabled = count[6] > 0;
            textBoxH.IsEnabled = count[7] > 0;
            textBoxI.IsEnabled = count[8] > 0;
            textBoxJ.IsEnabled = count[9] > 0;
            textBoxK.IsEnabled = count[10] > 0;
            textBoxL.IsEnabled = count[11] > 0;
            textBoxM.IsEnabled = count[12] > 0;
            textBoxN.IsEnabled = count[13] > 0;
            textBoxO.IsEnabled = count[14] > 0;
            textBoxP.IsEnabled = count[15] > 0;
            textBoxQ.IsEnabled = count[16] > 0;
            textBoxR.IsEnabled = count[17] > 0;
            textBoxS.IsEnabled = count[18] > 0;
            textBoxT.IsEnabled = count[19] > 0;
            textBoxU.IsEnabled = count[20] > 0;
            textBoxV.IsEnabled = count[21] > 0;
            textBoxW.IsEnabled = count[22] > 0;
            textBoxX.IsEnabled = count[23] > 0;
            textBoxY.IsEnabled = count[24] > 0;
            textBoxZ.IsEnabled = count[25] > 0;

            // Display counts of characters next to the textboxes
            labelA.Content = labelA.Content + "  :  " + count[0];
            labelB.Content = labelB.Content + "  :  " + count[1];
            labelC.Content = labelC.Content + "  :  " + count[2];
            labelD.Content = labelD.Content + "  :  " + count[3];
            labelE.Content = labelE.Content + "  :  " + count[4];
            labelF.Content = labelF.Content + "  :  " + count[5];
            labelG.Content = labelG.Content + "  :  " + count[6];
            labelH.Content = labelH.Content + "  :  " + count[7];
            labelI.Content = labelI.Content + "  :  " + count[8];
            labelJ.Content = labelJ.Content + "  :  " + count[9];
            labelK.Content = labelK.Content + "  :  " + count[10];
            labelL.Content = labelL.Content + "  :  " + count[11];
            labelM.Content = labelM.Content + "  :  " + count[12];
            labelN.Content = labelN.Content + "  :  " + count[13];
            labelO.Content = labelO.Content + "  :  " + count[14];
            labelP.Content = labelP.Content + "  :  " + count[15];
            labelQ.Content = labelQ.Content + "  :  " + count[16];
            labelR.Content = labelR.Content + "  :  " + count[17];
            labelS.Content = labelS.Content + "  :  " + count[18];
            labelT.Content = labelT.Content + "  :  " + count[19];
            labelU.Content = labelU.Content + "  :  " + count[20];
            labelV.Content = labelV.Content + "  :  " + count[21];
            labelW.Content = labelW.Content + "  :  " + count[22];
            labelX.Content = labelX.Content + "  :  " + count[23];
            labelY.Content = labelY.Content + "  :  " + count[24];
            labelZ.Content = labelZ.Content + "  :  " + count[25];

            /*
            String output = "Counts: ";
            
            foreach(int i in count) {
                output+= i + " ";
            }
            MessageBox.Show(output); 
             */
        }
    }
}
